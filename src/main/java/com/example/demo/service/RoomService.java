package com.example.demo.service;

import com.example.demo.model.Room;
import com.example.demo.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoomService {

    private final RoomRepository roomRepository;

    @Autowired
    public RoomService(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    public List<Room> getAllRooms() {
        return roomRepository.findAll();
    }

    public Room getRoomById(Long roomId) {
        return roomRepository.findById(roomId).orElse(null);
    }

    public Room createRoom(Room room) {
        return roomRepository.save(room);
    }

    public Room updateRoom(Long roomId, Room updatedRoom) {
        Room existingRoom = roomRepository.findById(roomId).orElse(null);
        if (existingRoom != null) {
            existingRoom.setRoomNumber(updatedRoom.getRoomNumber());
            existingRoom.setRoomType(updatedRoom.getRoomType());
            existingRoom.setType(updatedRoom.getType());
            existingRoom.setAvailable(updatedRoom.isAvailable());
            existingRoom.setPrice(updatedRoom.getPrice());
            // Set other properties as needed
            return roomRepository.save(existingRoom);
        }
        return null;
    }

    public boolean deleteRoom(Long roomId) {
        Room existingRoom = roomRepository.findById(roomId).orElse(null);
        if (existingRoom != null) {
            roomRepository.delete(existingRoom);
            return true;
        }
        return false;
    }

    // Add other methods for room exploration, additional facilities selection, etc.

}
