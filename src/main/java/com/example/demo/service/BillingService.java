package com.example.demo.service;

import com.example.demo.model.Billing;
import com.example.demo.repository.BillingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BillingService {
    private final BillingRepository billingRepository;

    @Autowired
    public BillingService(BillingRepository billingRepository) {
        this.billingRepository = billingRepository;
    }

    public List<Billing> getAllBilling() {
        return billingRepository.findAll();
    }

    public Billing getBillingById(Long id) {
        return billingRepository.findById(id).orElse(null);
    }

    public Billing createBilling(Billing billing) {
        return billingRepository.save(billing);
    }

    public Billing updateBilling(Long id, Billing billing) {
        Billing existingBilling = billingRepository.findById(id).orElse(null);
        if (existingBilling != null) {
            existingBilling.setCheckInDate(billing.getCheckInDate());
            existingBilling.setCheckOutDate(billing.getCheckOutDate());
            existingBilling.setRoom(billing.getRoom());
            existingBilling.setUser(billing.getUser());
            return billingRepository.save(existingBilling);
        }
        return null;
    }

    public void deleteBilling(Long id) {
        billingRepository.deleteById(id);
    }
}
