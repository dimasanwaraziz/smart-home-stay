package com.example.demo.service;

import com.example.demo.model.AdditionalFacility;
import com.example.demo.repository.AdditionalFacilityRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdditionalFacilityService {
    private final AdditionalFacilityRepository additionalFacilityRepository;

    public AdditionalFacilityService(AdditionalFacilityRepository additionalFacilityRepository) {
        this.additionalFacilityRepository = additionalFacilityRepository;
    }

    public AdditionalFacility createAdditionalFacility(AdditionalFacility additionalFacility) {
        return additionalFacilityRepository.save(additionalFacility);
    }

    public AdditionalFacility getAdditionalFacilityById(Long id) {
        return additionalFacilityRepository.findById(id).orElse(null);
    }

    public List<AdditionalFacility> getAllAdditionalFacilities() {
        return additionalFacilityRepository.findAll();
    }

    public AdditionalFacility updateAdditionalFacility(Long id, AdditionalFacility additionalFacility) {
        AdditionalFacility existingAdditionalFacility = additionalFacilityRepository.findById(id).orElse(null);
        if (existingAdditionalFacility != null) {
            existingAdditionalFacility.setName(additionalFacility.getName());
            existingAdditionalFacility.setDescription(additionalFacility.getDescription());
            existingAdditionalFacility.setPrice(additionalFacility.getPrice());
            return additionalFacilityRepository.save(existingAdditionalFacility);
        }
        return null;
    }

    public boolean deleteAdditionalFacility(Long id) {
        AdditionalFacility existingAdditionalFacility = additionalFacilityRepository.findById(id).orElse(null);
        if (existingAdditionalFacility != null) {
            additionalFacilityRepository.delete(existingAdditionalFacility);
            return true;
        }
        return false;
    }
}
