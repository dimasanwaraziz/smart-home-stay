package com.example.demo.service;

import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User registerUser(User user) {
        // Lakukan validasi atau operasi lain yang diperlukan sebelum mendaftarkan pengguna
        // Misalnya, validasi email atau enkripsi password sebelum menyimpan pengguna ke dalam database
        // ...
        
        // Simpan pengguna ke dalam database
        return userRepository.save(user);
    }

    public User getUserById(Long userId) {
        return userRepository.findById(userId).orElse(null);
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public User updateUser(Long userId, User updatedUser) {
        User user = userRepository.findById(userId).orElse(null);
        if (user != null) {
            // Lakukan validasi atau operasi lain yang diperlukan sebelum memperbarui pengguna
            // ...
            
            // Update data pengguna dengan data yang diberikan
            user.setName(updatedUser.getName());
            user.setEmail(updatedUser.getEmail());
            // Update properti lainnya sesuai kebutuhan
            
            return userRepository.save(user);
        }
        return null;
    }

    public boolean deleteUser(Long userId) {
        User user = userRepository.findById(userId).orElse(null);
        if (user != null) {
            userRepository.delete(user);
            return true;
        }
        return false;
    }

    // Add other methods for check-in, check-out, etc.

}
