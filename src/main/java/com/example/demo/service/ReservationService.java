package com.example.demo.service;

import com.example.demo.model.Reservation;
import com.example.demo.model.Room;
import com.example.demo.model.User;
import com.example.demo.repository.ReservationRepository;
import com.example.demo.repository.RoomRepository;
import com.example.demo.repository.UserRepository;

import org.springframework.stereotype.Service;

import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;

@Service
public class ReservationService {
    private final ReservationRepository reservationRepository;
    private final UserRepository userRepository;
    private final RoomRepository roomRepository;

    public ReservationService(ReservationRepository reservationRepository, UserRepository userRepository,
            RoomRepository roomRepository) {
        this.reservationRepository = reservationRepository;
        this.userRepository = userRepository;
        this.roomRepository = roomRepository;
    }

    public Reservation createReservation(Reservation reservation) {
        User user = userRepository.findById(reservation.getUser().getId()).orElse(null);
        Room room = roomRepository.findById(reservation.getRoom().getId()).orElse(null);

        if (user != null && room != null) {
            reservation.setUser(user);
            reservation.setRoom(room);
            return reservationRepository.save(reservation);
        }

        return null;
    }

    public Reservation getReservationById(Long id) {
        return reservationRepository.findById(id).orElse(null);
    }

    public List<Reservation> getAllReservations() {
        return reservationRepository.findAll();
    }

    public Reservation updateReservation(Long id, Reservation reservation) {
        if (reservationRepository.existsById(id)) {
            Reservation existingReservation = reservationRepository.findById(id).orElse(null);
            if (existingReservation != null) {
                User user = userRepository.findById(reservation.getUser().getId()).orElse(null);
                Room room = roomRepository.findById(reservation.getRoom().getId()).orElse(null);
                if (user != null && room != null) {
                    existingReservation.setCheckInDate(reservation.getCheckInDate());
                    existingReservation.setCheckOutDate(reservation.getCheckOutDate());
                    existingReservation.setNumberOfGuests(reservation.getNumberOfGuests());
                    existingReservation.setUser(user);
                    existingReservation.setRoom(room);
                    return reservationRepository.save(existingReservation);
                }
            }
        }
        return null;
    }

    public boolean deleteReservation(Long id) {
        if (reservationRepository.existsById(id)) {
            reservationRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public double calculateReservationBilling(Reservation reservation) {
        // Implementasi logika perhitungan billing reservasi di sini
        // Misalnya, menghitung jumlah hari menginap dikali harga kamar

        Date checkInDate = reservation.getCheckInDate();
        Date checkOutDate = reservation.getCheckOutDate();
        double roomPrice = reservation.getRoom().getPrice();

        // Contoh perhitungan sederhana: jumlah hari menginap dikali harga kamar
        long numberOfDays = ChronoUnit.DAYS.between(checkInDate.toInstant(), checkOutDate.toInstant());
        double billingAmount = numberOfDays * roomPrice;

        return billingAmount;
    }

}
