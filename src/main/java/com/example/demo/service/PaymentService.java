package com.example.demo.service;

import com.example.demo.model.Payment;
import com.example.demo.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentService {
    private final PaymentRepository paymentRepository;

    @Autowired
    public PaymentService(PaymentRepository paymentRepository) {
        this.paymentRepository = paymentRepository;
    }

    public Payment makePayment(Payment payment) {
        // Logika bisnis untuk pembuatan pembayaran
        // Misalnya, menghitung total pembayaran, menghubungkan dengan metode pembayaran eksternal, dll.
        // Anda dapat menyesuaikan logika ini sesuai dengan kebutuhan aplikasi Anda
        // Contoh sederhana di bawah ini hanya menyimpan pembayaran ke dalam repository

        return paymentRepository.save(payment);
    }

    public Payment getPaymentById(Long id) {
        return paymentRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Payment not found"));
    }

    public List<Payment> getAllPayments() {
        return paymentRepository.findAll();
    }

    public Payment updatePayment(Long id, Payment payment) {
        Payment existingPayment = getPaymentById(id);

        // Update properti pembayaran yang ingin diubah
        // Contoh sederhana di bawah ini hanya mengubah status pembayaran
        existingPayment.setStatus(payment.getStatus());

        return paymentRepository.save(existingPayment);
    }

    public void deletePayment(Long id) {
        Payment payment = getPaymentById(id);
        paymentRepository.delete(payment);
    }

    // Other service methods for additional operations

}
