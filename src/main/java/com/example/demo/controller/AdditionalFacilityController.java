package com.example.demo.controller;

import com.example.demo.model.AdditionalFacility;
import com.example.demo.service.AdditionalFacilityService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/additional-facilities")
public class AdditionalFacilityController {
    private final AdditionalFacilityService additionalFacilityService;

    public AdditionalFacilityController(AdditionalFacilityService additionalFacilityService) {
        this.additionalFacilityService = additionalFacilityService;
    }

    @PostMapping
    public ResponseEntity<AdditionalFacility> createAdditionalFacility(@RequestBody AdditionalFacility additionalFacility) {
        AdditionalFacility createdAdditionalFacility = additionalFacilityService.createAdditionalFacility(additionalFacility);
        return ResponseEntity.ok(createdAdditionalFacility);
    }

    @GetMapping("/{id}")
    public ResponseEntity<AdditionalFacility> getAdditionalFacilityById(@PathVariable Long id) {
        AdditionalFacility additionalFacility = additionalFacilityService.getAdditionalFacilityById(id);
        if (additionalFacility == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(additionalFacility);
    }

    @GetMapping
    public ResponseEntity<List<AdditionalFacility>> getAllAdditionalFacilities() {
        List<AdditionalFacility> additionalFacilities = additionalFacilityService.getAllAdditionalFacilities();
        return ResponseEntity.ok(additionalFacilities);
    }

    @PutMapping("/{id}")
    public ResponseEntity<AdditionalFacility> updateAdditionalFacility(@PathVariable Long id, @RequestBody AdditionalFacility additionalFacility) {
        AdditionalFacility updatedAdditionalFacility = additionalFacilityService.updateAdditionalFacility(id, additionalFacility);
        if (updatedAdditionalFacility == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(updatedAdditionalFacility);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteAdditionalFacility(@PathVariable Long id) {
        boolean deleted = additionalFacilityService.deleteAdditionalFacility(id);
        if (!deleted) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.noContent().build();
    }
}
