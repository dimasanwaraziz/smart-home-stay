package com.example.demo.controller;

import com.example.demo.model.Reservation;
import com.example.demo.service.ReservationService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/reservations")
public class ReservationController {
    private final ReservationService reservationService;

    public ReservationController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @PostMapping
    public ResponseEntity<Reservation> createReservation(@RequestBody Reservation reservation) {
        Reservation createdReservation = reservationService.createReservation(reservation);
        return ResponseEntity.ok(createdReservation);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Reservation> getReservationById(@PathVariable Long id) {
        Reservation reservation = reservationService.getReservationById(id);
        if (reservation == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(reservation);
    }

    @GetMapping
    public ResponseEntity<List<Reservation>> getAllReservations() {
        List<Reservation> reservations = reservationService.getAllReservations();
        return ResponseEntity.ok(reservations);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Reservation> updateReservation(@PathVariable Long id, @RequestBody Reservation reservation) {
        Reservation updatedReservation = reservationService.updateReservation(id, reservation);
        if (updatedReservation == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(updatedReservation);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteReservation(@PathVariable Long id) {
        boolean deleted = reservationService.deleteReservation(id);
        if (!deleted) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}/billing")
    public ResponseEntity<Double> calculateReservationBilling(@PathVariable Long id) {
        Reservation reservation = reservationService.getReservationById(id);

        if (reservation == null) {
            return ResponseEntity.notFound().build();
        }

        double billingAmount = reservationService.calculateReservationBilling(reservation);

        return ResponseEntity.ok(billingAmount);
    }

    @PostMapping("/{id}/check-in")
    public ResponseEntity<String> checkInReservation(@PathVariable Long id) {
        Reservation reservation = reservationService.getReservationById(id);

        if (reservation == null) {
            return ResponseEntity.notFound().build();
        }

        if (reservation.isCheckedIn()) {
            return ResponseEntity.badRequest().body("Reservation is already checked in");
        }

        reservation.setCheckedIn(true);
        reservationService.updateReservation(id, reservation);

        return ResponseEntity.ok("Reservation checked in successfully");
    }

    @PutMapping("/{id}/check-out")
    public ResponseEntity<String> checkoutReservation(@PathVariable Long id) {
        Reservation reservation = reservationService.getReservationById(id);

        if (reservation == null) {
            return ResponseEntity.notFound().build();
        }

        // Perform the checkout logic
        reservation.setCheckedOut(true);

        // Update the reservation in the database
        Reservation updatedReservation = reservationService.updateReservation(id, reservation);

        if (updatedReservation != null) {
            return ResponseEntity.ok("Checkout successful");
        } else {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping("/{id}/pay")
    public ResponseEntity<String> payReservation(@PathVariable Long id) {
        Reservation reservation = reservationService.getReservationById(id);
        if (reservation == null) {
            return ResponseEntity.notFound().build();
        }

        if (reservation.isPaid()) {
            return ResponseEntity.badRequest().body("Reservation has already been paid.");
        }

        // Perform payment processing logic here

        // Update the reservation as paid
        reservation.setPaid(true);
        reservationService.updateReservation(id, reservation);

        return ResponseEntity.ok("Reservation successfully paid.");
    }
}
