package com.example.demo.controller;

import com.example.demo.model.Payment;
import com.example.demo.model.Reservation;
import com.example.demo.service.PaymentService;
import com.example.demo.service.ReservationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/payments")
public class PaymentController {
    private final PaymentService paymentService;
    private final ReservationService reservationService;

    @Autowired
    public PaymentController(PaymentService paymentService, ReservationService reservationService) {
        this.paymentService = paymentService;
        this.reservationService = reservationService;
    }

    @PostMapping
    public ResponseEntity<Payment> makePayment(@RequestBody Payment payment) {
        Payment newPayment = paymentService.makePayment(payment);
        return ResponseEntity.status(HttpStatus.CREATED).body(newPayment);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Payment> getPaymentById(@PathVariable Long id) {
        Payment payment = paymentService.getPaymentById(id);
        return ResponseEntity.ok(payment);
    }

    @GetMapping
    public ResponseEntity<List<Payment>> getAllPayments() {
        List<Payment> payments = paymentService.getAllPayments();
        return ResponseEntity.ok(payments);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Payment> updatePayment(@PathVariable Long id, @RequestBody Payment payment) {
        Payment updatedPayment = paymentService.updatePayment(id, payment);
        return ResponseEntity.ok(updatedPayment);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePayment(@PathVariable Long id) {
        paymentService.deletePayment(id);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/{reservationId}")
    public ResponseEntity<String> makePayment(@PathVariable Long reservationId) {
        // Retrieve the reservation from the database
        Reservation reservation = reservationService.getReservationById(reservationId);

        // Check if the reservation exists
        if (reservation == null) {
            return ResponseEntity.notFound().build();
        }

        // Perform payment processing logic here
        // ...

        // Update the reservation to mark it as paid
        reservation.setPaid(true);
        reservationService.updateReservation(reservationId, reservation);

        return ResponseEntity.ok("Payment successful");
    }

    // Other controller methods for additional operations

}
