package com.example.demo.repository;

import com.example.demo.model.AdditionalFacility;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AdditionalFacilityRepository extends JpaRepository<AdditionalFacility, Long> {
}
