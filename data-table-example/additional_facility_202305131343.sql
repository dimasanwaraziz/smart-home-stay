INSERT INTO spring.additional_facility (name,price,description) VALUES
	 ('Breakfast',10.0,'Delicious breakfast meal'),
	 ('Airport Shuttle',25.0,'Transportation service to and from the airport'),
	 ('Spa Package',50.0,'Relaxing spa treatment package'),
	 ('Extra Bed',20.0,'Additional bed for extra comfort');
