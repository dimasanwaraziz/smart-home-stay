# Smart Home Stay API

API Smart Home Stay adalah aplikasi berbasis RESTful yang menyediakan layanan untuk mengelola pemesanan, fasilitas tambahan, dan pengguna dalam sistem Smart Home Stay.

## Daftar Isi

- [Instalasi](#instalasi)
- [Menjalankan Aplikasi](#menjalankan-aplikasi)
- [Endpoint](#endpoint)
- [Contoh Penggunaan](#contoh-penggunaan)
- [Kontribusi](#kontribusi)
- [Pertanyaan](#pertanyaan)
- [Dukungan](#dukungan)

## Instalasi

1. Pastikan Anda memiliki JDK (Java Development Kit) terinstal di komputer Anda.
2. Clone repositori ini ke komputer Anda.
3. Buka proyek di lingkungan pengembangan Java favorit Anda (misalnya, IntelliJ IDEA, Eclipse).
4. Pastikan konfigurasi dependensi Maven sudah benar dalam file `pom.xml`.
5. Atur koneksi database MySQL Anda dalam file `application.properties`.
6. Lakukan proses build proyek untuk mengunduh dependensi yang diperlukan.

## Menjalankan Aplikasi

1. Setelah proses build selesai, jalankan aplikasi dengan menjalankan kelas `com.example.demo.DemoApplication`.
2. Aplikasi akan dijalankan di server lokal dengan URL `http://localhost:8080`.

## Endpoint

Berikut adalah daftar endpoint yang disediakan oleh API Smart Home Stay:

Users:

- `GET /api/users/{id}`: Mendapatkan informasi pengguna berdasarkan ID.
- `PUT /api/users/{id}`: Mengupdate informasi pengguna berdasarkan ID.
- `DELETE /api/users/{id}`: Menghapus pengguna berdasarkan ID.
- `POST /api/users/register`: Mendaftarkan pengguna baru.
- `GET /api/users/register`: Melihat semua pengguna.

Rooms:

- `GET /api/rooms/{id}`: Mendapatkan informasi kamar berdasarkan ID.
- `PUT /api/rooms/{id}`: Mengupdate informasi kamar berdasarkan ID.
- `DELETE /api/rooms/{id}`: Menghapus kamar berdasarkan ID.
- `POST /api/rooms`: Menambahkan kamar baru.
- `GET /api/rooms`: Mendapatkan daftar semua kamar yang tersedia.

Reservations:

- `GET /api/reservations/{id}`: Mendapatkan informasi reservasi berdasarkan ID.
- `PUT /api/reservations/{id}`: Mengupdate informasi reservasi berdasarkan ID.
- `DELETE /api/reservations/{id}`: Membatalkan reservasi berdasarkan ID.
- `PUT /api/reservations/{id}/check-out`: Melakukan check-out untuk reservasi berdasarkan ID.
- `GET /api/reservations`: Mendapatkan daftar semua reservasi.
- `POST /api/reservations`: Membuat reservasi baru.
- `POST /api/reservations/{id}/pay`: Melakukan pembayaran untuk reservasi berdasarkan ID.
- `POST /api/reservations/{id}/check-in`: Melakukan check-in untuk reservasi berdasarkan ID.
- `POST /api/reservations/{id}/payment`: Melakukan pembayaran untuk reservasi berdasarkan ID.
- `GET /api/reservations/{id}/billing`: Melihat total harga untuk reservasi berdasarkan ID.

Payment:

- `GET /api/payments/{id}`: Mendapatkan informasi pembayaran berdasarkan ID.
- `PUT /api/payments/{id}`: Mengupdate informasi pembayaran berdasarkan ID.
- `DELETE /api/payments/{id}`: Menghapus pembayaran berdasarkan ID.
- `GET /api/payments`: Mendapatkan semua pembayaran.
- `POST /api/payments`: Menambahkan pembayaran baru.
- `POST /api/payments/{reservationId}`: Melakukan pembayaran berdasarkan reservationId.

Billing:

- `GET /api/billing/{id}`: Mendapatkan informasi billing berdasarkan ID reservasi.
- `PUT /api/billing/{id}`: Mengupdate informasi billing berdasarkan ID reservasi.
- `DELETE /api/billing/{id}`: Menghapus informasi billing berdasarkan ID reservasi.
- `GET /api/billing`: Mendapatkan informasi billing semua billing.
- `POST /api/billing/{id}`: Membuat informasi billing berdasarkan ID reservasi.

Additional Facilities:

- `GET /api/additional-facilities/{id}`: Mendapatkan informasi fasilitas tambahan berdasarkan ID.
- `PUT /api/additional-facilities/{id}`: Mengupdate informasi fasilitas tambahan berdasarkan ID.
- `DELETE /api/additional-facilities/{id}`: Menghapus fasilitas tambahan berdasarkan ID.
- `GET /api/additional-facilities`: Mendapatkan daftar semua fasilitas tambahan yang tersedia.
- `POST /api/additional-facilities`: Menambahkan fasilitas tambahan baru.

Untuk versi lengkap bisa mengguankan swagger ui pada alamat `/swagger-ui/index.html` ketika anda menjalankan program.

## Contoh Penggunaan

### Register user

Request:

`POST /api/users/register`

Body Request:

```
{
  "username": "JohnDoe",
  "name": "John Doe",
  "password": "mypassword",
  "email": "johndoe@example.com"
}
```

Response:
```
{
  "id": 1,
  "name": "John Doe",
  "email": "johndoe@example.com",
  "password": "mypassword"
}
```

### Membuat reservasi baru

Request:

`POST /api/reservations`

Body Request:

```
{
  "checkInDate": "2023-01-01",
  "checkOutDate": "2023-01-05",
  "numberOfGuests": 2,
  "user": {
    "id": 1
  },
  "room": {
    "id": 1
  }
}
```

Response:

```
{
  "id": 3,
  "checkInDate": "2023-01-01T00:00:00.000+00:00",
  "checkOutDate": "2023-01-05T00:00:00.000+00:00",
  "numberOfGuests": 2,
  "checkedIn": false,
  "checkedOut": false,
  "paid": false,
  "user": {
    "id": 1,
    "name": "John Doe",
    "email": "johndoe@example.com",
    "password": "mypassword"
  },
  "room": {
    "id": 1,
    "type": "Single",
    "price": 60.0,
    "available": true,
    "roomNumber": "101",
    "roomType": "Single"
  }
}
```

### Staff menghitung billing

Request:

`GET /api/reservations/{reservationId}/billing`


Response:

```
240.0
```

### Melakukan pembayaran reservasi

Request:

`POST /api/reservations/{reservationId}/pay`


Response:

```
Reservation successfully paid.
```

### User melakukan check-in

Request:

`POST /api/reservations/{reservationId}/check-in`

Response:

```
Reservation checked in successfully
```

### User melakukan check-out

Request:

`POST /api/reservations/{reservationId}/check-out`

Response:

```
Checkout successful
```

Dengan menggunakan contoh-contoh di atas, Anda dapat melihat bagaimana melakukan permintaan HTTP ke API Smart Home Stay untuk berbagai operasi yang tersedia. Pastikan untuk mengganti {id} dengan ID yang sesuai sesuai dengan kebutuhan Anda.